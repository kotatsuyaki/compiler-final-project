.PHONY: all clean test

CC := gcc
CXX := g++
CXXFLAGS := -O3 -std=c++11

CFLAGS += -O3
YACC_BIN = byacc
YACC_FLAGS = -dv

BINARY = codegen

$(BINARY): y.tab.c y.tab.h lex.yy.c support.h support.cpp gen.cpp gen.h
	$(CXX) gen.cpp support.cpp lex.yy.c y.tab.c $(CXXFLAGS) -o $@
	chmod a+x $(BINARY)

lex.yy.c: scanner.l
	flex scanner.l

y.tab.c y.tab.h: parser.y
	$(YACC_BIN) $(YACC_FLAGS) $<

clean:
	rm -rf lex.yy.c y.tab.c y.tab.h $(BINARY)
