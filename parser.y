%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "support.h"
#include "gen.h"

#include <iostream>

#ifndef YYSTYPE
#define YYSTYPE Node*
#endif

int yylex(void);

static void output_list(Node* head);
%}

%token TYPE IDENT INTLIT FLTLIT CHARLIT STRLIT CONST IF ELSE SWT CASE DFT WHL DO FOR BRK RET CONT

%nonassoc NIMP

%right '='
%left LOR
%left LAND
%left '|'
%left '&'
%left EQ NEQ
%left '<' LEQ '>' GEQ
%left '+' '-'
%left '*' '/' '%'
%right '!'
%right INC DEC
%nonassoc SUFINC SUFDEC

%nonassoc UMINUS UPLUS
%nonassoc IMP

%%

program: global {
    auto global_head = new Node(global_head_str);
    auto global_tail = new Node(additional_fn);
    $$ = concat(3, global_head, $1, global_tail);
    node_print_list_dbg("program <= global", $$);
    output_list($$);
    output_to_file($$);
}

global: /* empty */ {
    $$ = NULL;
} | global scalar_decl {
    if ($1 == NULL)
        $$ = $2;
    else
        $$ = concat(2, $1, $2);
    node_print_list_dbg("global <= scalar_decl", $$);
} | global array_decl {
    if ($1 == NULL)
        $$ = $2;
    else
        $$ = concat(2, $1, $2);
    node_print_list_dbg("global <= array_decl", $$);
} | global funct_decl {
    $$ = new Node("");
    node_print_list_dbg("global <= funct_decl", $$);
} | global stmt {
    if ($1 == NULL)
        $$ = $2;
    else
        $$ = concat(2, $1, $2);
    node_print_list_dbg("global <= stmt", $$);
} | global const_decl {
    if ($1 == NULL)
        $$ = $2;
    else
        $$ = concat(2, $1, $2);
    node_print_list_dbg("global <= const_decl", $$);
} | global funct_def {
    if ($1 == NULL)
        $$ = $2;
    else
        $$ = concat(2, $1, $2);
    node_print_list_dbg("global <= funct_def", $$);
}

 /* Statement */
stmt: expr ';' {
    $$ = $1;
    node_print_list_dbg("stmt <= expr ;", $$);
} | comp_stmt {
    $$ = $1;
    node_print_list_dbg("stmt <= comp_stmt", $$);
} | if_stmt {
    $$ = concat(2, new Node("# IF statment begin\n"), $1);

    node_print_list_dbg("stmt <= if_stmt", $$);
} | if_else_stmt {
    $$ = concat(2, new Node("# IF-else statment begin\n"), $1);

    node_print_list_dbg("stmt <= if_else_stmt", $$);
} | swt_stmt {
    Node* opentag = node_new_clone_str("<stmt>");
    Node* closetag = node_new_clone_str("</stmt>");

    $$ = concat(3, opentag, $1, closetag);

    node_print_list_dbg("stmt <= swt_stmt", $$);
} | while_stmt {
    $$ = concat(2, new Node("# while statement begin\n"), $1);

    node_print_list_dbg("stmt <= while_stmt", $$);
} | dowhile_stmt {
    $$ = concat(2, new Node("# do-while statement begin\n"), $1);

    node_print_list_dbg("stmt <= dowhile_stmt", $$);
} | for_stmt {
    Node* opentag = node_new_clone_str("<stmt>");
    Node* closetag = node_new_clone_str("</stmt>");

    $$ = concat(3, opentag, $1, closetag);

    node_print_list_dbg("stmt <= for_stmt", $$);
} | ret_stmt {
    Node* opentag = node_new_clone_str("<stmt>");
    Node* closetag = node_new_clone_str("</stmt>");

    $$ = concat(3, opentag, $1, closetag);

    node_print_list_dbg("stmt <= ret_stmt", $$);
} | brk_stmt {
    Node* opentag = node_new_clone_str("<stmt>");
    Node* closetag = node_new_clone_str("</stmt>");

    $$ = concat(3, opentag, $1, closetag);

    node_print_list_dbg("stmt <= brk_stmt", $$);
} | cont_stmt {
    Node* opentag = node_new_clone_str("<stmt>");
    Node* closetag = node_new_clone_str("</stmt>");

    $$ = concat(3, opentag, $1, closetag);

    node_print_list_dbg("stmt <= cont_stmt", $$);
}

comp_stmt: '{' comp_stmt_inner '}' {
    $$ = $2;

    node_print_list_dbg("comp_stmt <= { comp_stmt_inner }", $$);

    if ($$ == NULL) {
        std::cerr << "comp_stmt IS NULL\n";
    }
}

comp_stmt_inner: /* empty */ {
    $$ = NULL;
} | comp_stmt_inner stmt {
    if ($1 == NULL) {
        $$ = $2;
    } else {
        $$ = concat(2, $1, $2);
    }
} | comp_stmt_inner scalar_decl {
    if ($1 == NULL) {
        $$ = $2;
        std::cerr << "1 null case\n";
    } else {
        $$ = concat(2, $1, $2);
        std::cerr << "2 not null case\n";
    }
    node_print_list_dbg("comp_stmt_inner <= comp_stmt_inner scalar_decl", $$);
} | comp_stmt_inner const_decl {
    if ($1 == NULL) {
        $$ = $2;
    } else {
        $$ = concat(2, $1, $2);
    }
    node_print_list_dbg("comp_stmt_inner <= comp_stmt_inner const_decl", $$);
}

 /* Expression */

expr: expr '+' expr {
    std::stringstream ss;
    ss << "# ADD\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "add t0, t0, t1\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $1, $3, op);
} | expr '-' expr {
    std::stringstream ss;
    ss << "# SUB\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "sub t0, t0, t1\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $1, $3, op);
} | expr '*' expr {
    std::stringstream ss;
    ss << "# MUL\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "mul t0, t0, t1\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $1, $3, op);
} | expr '/' expr {
    std::stringstream ss;
    ss << "# DIV\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "div t0, t0, t1\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $1, $3, op);
} | expr '&' expr {
    std::stringstream ss;
    ss << "# AND\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "and t0, t0, t1\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $1, $3, op);
} | expr '|' expr {
    std::stringstream ss;
    ss << "# OR\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "or t0, t0, t1\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $1, $3, op);
} | expr '%' expr {
    std::stringstream ss;
    ss << "# REM\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "rem t0, t0, t1\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $1, $3, op);
} | expr '<' expr {
    std::stringstream ss;
    ss << "# LT\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "slt t0, t0, t1\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $1, $3, op);
} | expr '>' expr {
    std::stringstream ss;
    ss << "# GT\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "slt t0, t1, t0\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $1, $3, op);
} | expr GEQ expr {
    std::stringstream ss;
    ss << "# GEQ\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "slt t0, t0, t1\n";
    ss << "xori t0, t0, 1\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $1, $3, op);
} | expr LEQ expr {
    std::stringstream ss;
    ss << "# LEQ\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "slt t0, t1, t0\n";
    ss << "xori t0, t0, 1\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $1, $3, op);
} | expr NEQ expr {
    std::stringstream ss;
    ss << "# NEQ\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "sub t0, t0, t1\n";
    ss << "snez t0, t0\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $1, $3, op);
} | expr EQ expr {
    std::stringstream ss;
    ss << "# EQ\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "sub t0, t0, t1\n";
    ss << "seqz t0, t0\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $1, $3, op);
} | expr LOR expr {
    std::stringstream ss;
    ss << "# LOR\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "or t0, t0, t1\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    $$ = new Node(ss.str());
} | expr LAND expr {
    std::stringstream ss;
    ss << "# LAND\n";
    ss << "lw t1, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "and t0, t0, t1\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $1, $3, op);
} | '-' expr %prec UMINUS {
    std::stringstream ss;
    ss << "# UMINUS\n";
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "li t1, 0\n"; // t1 is 0
    ss << "sub t0, t1, t0\n"; // t0 <= 0 - t0
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(3, $2, op);
} | '+' expr %prec UPLUS {
} | '!' expr {
    std::stringstream ss;
    ss << "lw t0, 0(sp)\n";
    ss << "addi sp, sp, 4\n";
    ss << "seqz t0, t0\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    Node* op = new Node(ss.str());
    $$ = concat(2, $2, op);
} | literal {
    $$ = $1;
    node_print_list_dbg("expr <= literal", $$);
} | variable {
    $$ = get_loadvar_node($1->str);
} | variable INC %prec SUFINC {
    auto local_pos = var_table_get($1->str);

    std::stringstream ss;
    ss << "# POSTINC " << $1->str << "\n";
    // Load variable
    ss << "lw t0, -" << (local_pos * 4 + 48) << "(s0)\n";
    ss << "mv t1, t0\n"; // t1 holds x
    ss << "addi t0, t0, 1\n"; // t0 holds x + 1
    ss << "sw t1, -4(sp)\n"; // push x
    ss << "addi sp, sp, -4\n";
    ss << "sw t0, -" << (local_pos * 4 + 48) << "(s0)\n"; // Write x + 1 back

    $$ = new Node(ss.str());
} | INC variable {
    auto local_pos = var_table_get($2->str);

    std::stringstream ss;
    ss << "# PREINC " << $1->str << "\n";
    // Load variable
    ss << "lw t0, -" << (local_pos * 4 + 48) << "(s0)\n";
    ss << "addi t0, t0, 1\n"; // t0 holds x + 1
    ss << "sw t0, -4(sp)\n"; // push x + 1
    ss << "addi sp, sp, -4\n";
    ss << "sw t0, -" << (local_pos * 4 + 48) << "(s0)\n"; // Write x + 1 back

    $$ = new Node(ss.str());
} | variable DEC %prec SUFDEC {
    auto local_pos = var_table_get($1->str);

    std::stringstream ss;
    ss << "# POSTDEC " << $1->str << "\n";
    // Load variable
    ss << "lw t0, -" << (local_pos * 4 + 48) << "(s0)\n";
    ss << "mv t1, t0\n"; // t1 holds x
    ss << "addi t0, t0, -1\n"; // t0 holds x - 1
    ss << "sw t1, -4(sp)\n"; // push x
    ss << "addi sp, sp, -4\n";
    ss << "sw t0, -" << (local_pos * 4 + 48) << "(s0)\n"; // Write x - 1 back
    $$ = new Node(ss.str());
} | DEC variable {
    auto local_pos = var_table_get($2->str);

    std::stringstream ss;
    ss << "# PREDEC " << $1->str << "\n";
    // Load variable
    ss << "lw t0, -" << (local_pos * 4 + 48) << "(s0)\n";
    ss << "addi t0, t0, -1\n"; // t0 holds x - 1
    ss << "sw t0, -4(sp)\n"; // push x - 1
    ss << "addi sp, sp, -4\n";
    ss << "sw t0, -" << (local_pos * 4 + 48) << "(s0)\n"; // Write x - 1 back

    $$ = new Node(ss.str());
} | variable '=' expr {
    auto local_pos = var_table_get($1->str);

    std::stringstream ss;
    ss << "# Load variable " << $1->str << "\n";
    ss << "lw t0, -" << (local_pos * 4 + 48) << "(s0)\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    $$ = new Node(ss.str());
} | func_invoc {
    $$ = $1;

    node_print_list_dbg("expr <= func_invoc", $$);
} | CHARLIT {
    Node* opentag = node_new_clone_str("<expr>");
    Node* closetag = node_new_clone_str("</expr>");

    $$ = concat(3, opentag, $1, closetag);
} | STRLIT {
    Node* opentag = node_new_clone_str("<expr>");
    Node* closetag = node_new_clone_str("</expr>");

    $$ = concat(3, opentag, $1, closetag);
} | '(' expr')' %prec IMP {
    $$ = $2;
}

literal: INTLIT {
    int val = atoi($1->str.c_str());
    std::stringstream ss;
    ss << "# Load literal integer\n";
    ss << "li t0, " << val << "\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";
    delete $1;

    $$ = new Node(ss.str());
    node_print_list_dbg("literal <= INTLIT", $$);
} | FLTLIT {
    $$ = $1;
    node_print_list_dbg("literal <= FLTLIT", $$);
}

func_invoc: IDENT '(' func_args ')' {
    // The arguments is already in $$
    std::vector<Node*> nodes;

    if ($3) {
        nodes.push_back($3);
    }

    auto& key = $1->str;
    auto args_count = fn_table[key];
    
    // Pop arguments
    if (args_count != 0) {
        for (size_t ax = args_count; ax-- > 0; ) {
            std::stringstream ss;
            ss << "\n# Pop argument to a" << ax << "\n";
            ss << "lw a" << ax << ", 0(sp)\n";
            ss << "addi sp, sp, 4\n";
            nodes.push_back(new Node(ss.str()));
        }
    }

    std::stringstream ss;

    // Push old return address
    ss << "\n# Function has " << args_count << " arguments\n";
    ss << "\n# Push ra, jump, pop ra\n";
    ss << "sw ra, -4(sp)\n";
    ss << "addi sp, sp, -4\n";
    ss << "jal ra, " << $1->str << "\n";
    ss << "lw ra, 0(sp)\n";
    ss << "addi sp, sp, -4\n";

    nodes.push_back(new Node(ss.str()));

    if (nodes.size() == 1) {
        $$ = nodes[0];
    } else if (nodes.size() == 2) {
        $$ = concat(2, nodes[0], nodes[1]);
    } else if (nodes.size() == 3) {
        $$ = concat(3, nodes[0], nodes[1], nodes[2]);
    } else if (nodes.size() == 4) {
        $$ = concat(4, nodes[0], nodes[1], nodes[2], nodes[3]);
    } else {
        err("Too many arguments");
        exit(1);
    }
    
    node_print_list_dbg("func_invoc <= IDENT(func_args)", $$);
}

func_args: /* empty */ {
    $$ = NULL;
} | expr func_args_tail {
    if ($2 == NULL) {
        $$ = $1;
    } else {
        $$ = concat(2, $1, $2);
    }
    node_print_list_dbg("func_args <= expr func_args_tail", $$);
}

func_args_tail: /* empty */ {
    $$ = NULL;
} | ',' expr func_args_tail {
    if ($3 == NULL) {
        $$ = $2;
    } else {
        $$ = concat(2, $2, $3);
    }
}

variable: IDENT {
    $$ = $1;
    node_print_list_dbg("variable <= IDENT", $$);
} | IDENT variable_tail {
    $$ = concat(2, $1, $2);
    node_print_list_dbg("variable <= IDENT variable_tail", $$);
}

variable_tail: '[' expr ']' {
    Node* lp = node_new_clone_str("[");
    Node* rp = node_new_clone_str("]");

    $$ = concat(3, lp, $2, rp);
} | variable_tail '[' expr ']' {
    Node* lp = node_new_clone_str("[");
    Node* rp = node_new_clone_str("]");

    $$ = concat(4, $1, lp, $3, rp);
}

 /* Function declaration */

funct_decl: TYPE IDENT '(' params ')' ';' {
    fn_table[std::string($2->str)] = fn_decl_args_count;
    fn_decl_args_count = 0;
    std::cerr << "Function declared: " << $2->str << " (" << fn_table[std::string($2->str)] << ")\n";
}

params: /* empty */ {
    $$ = NULL;
} | param param_tail {
    fn_decl_args_count++;

    $$ = concat(2, $1, $2);
}

param_tail: /* empty */ {
    $$ = NULL;
} | ',' param param_tail {
    fn_decl_args_count++;
    $$ = concat(2, $2, $3);

    node_print_list_dbg("param_tail <= , parm param_tail", $$);
}

param: TYPE IDENT {
    $$ = $2;
    var_table_add($2->str);
    node_print_list_dbg("param <= TYPE IDENT", $$);
}

 /* Function definition */
funct_def: TYPE IDENT '(' params ')' {
    for (size_t i = 0; i < fn_decl_args_count; i++) {
        var_table[var_table.size() - i - 1].scope += 1;
        var_table[var_table.size() - i - 1].pos = fn_decl_args_count - i;
    }

    fn_locals_count = 0;
    cur_scope += 1;
} comp_stmt {
    Node* prolog = new Node(callee_entry_str);
    Node* epilog = new Node(callee_leave_str);

    Node* param_prolog = nullptr;
    Node* param_epilog = nullptr;

    {
        std::stringstream ss;

        ss << "# Param prolog\n";

        // Push args
        for (size_t i = 0; i < fn_decl_args_count; i++) {
            ss << "sw a" << i << ", -4(sp)\n";
            ss << "addi sp, sp, -4\n";
        }

        // Reserve space for locals
        ss << "addi sp, sp, -" << (4 * fn_locals_count) << "\n\n";

        param_prolog = new Node(ss.str());
    }

    {
        std::stringstream ss;

        // Pop off all args
        ss << "# Pop epilog\n";
        ss << "addi sp, sp, " << (4 * (fn_locals_count + fn_decl_args_count)) << "\n";

        param_epilog = new Node(ss.str());
    }

    $$ = concat(5, prolog, param_prolog, $7, param_epilog, epilog);
    if ($4) {
        delete $4;
    }

    node_print_list_dbg("funct_def <= TYPE IDENT ( params ) comp_stmt", $$);
    fn_decl_args_count = 0;
    cur_scope -= 1;
}

 /* Array declaration */

array_decl: TYPE arrays ';' {
    dbg("arrays_decl");
    Node* opentag = node_new_clone_str("<array_decl>");
    Node* closetag = node_new_clone_str(";</array_decl>");

    $$ = concat(4, opentag, $1, $2, closetag);
}

arrays: array {
    dbg("arrays 0");
    $$ = $1;
} | arrays ',' array {
    dbg("arrays 1");
    Node* delim = node_new_clone_str(",");
    $$ = concat(3, $1, delim, $3);
}

array: IDENT array_tails array_init {
    if ($3 == NULL) {
        $$ = concat(2, $1, $2);
    } else {
        $$ = concat(3, $1, $2, $3);
    }

    node_print_list_dbg("array <= IDENT array_tails array_init", $$);
}

array_tails: array_tails '[' signed_intlit ']' {
    dbg("array_tails 1");
    Node* lp = node_new_clone_str("[");
    Node* rp = node_new_clone_str("]");

    $$ = concat(4, $1, lp, $3, rp);
} | '[' signed_intlit ']' {
    dbg("array_tails 0");
    Node* lp = node_new_clone_str("[");
    Node* rp = node_new_clone_str("]");

    $$ = concat(3, lp, $2, rp);
}

signed_intlit: INTLIT {
    $$ = $1;
} | '+' INTLIT {
    int val = atoi($2->str.c_str());
    static char str[1000];
    sprintf(str, "li t0, %d\nsw t0, -4(sp)\naddi sp, sp, -4\n", val);

    delete $2;

    $$ = node_new_clone_str(str);
} | '-' INTLIT {
    int val = (-1) * atoi($2->str.c_str());
    static char str[1000];
    sprintf(str, "li t0, %d\nsw t0, -4(sp)\naddi sp, sp, -4\n", val);

    delete $2;

    $$ = node_new_clone_str(str);
}

array_init: /* empty */ {
    $$ = NULL;
} | '=' '{' array_init_list '}' {
    Node* lp = node_new_clone_str("={");
    Node* rp = node_new_clone_str("}");

    $$ = concat(3, lp, $3, rp);
}

array_init_list: expr {
    $$ = $1;

    node_print_list_dbg("array_init_list <= expr", $$);
} | '{' array_init_list '}' {
    Node* lp = node_new_clone_str("{");
    Node* rp = node_new_clone_str("}");

    $$ = concat(3, lp, $2, rp);

    node_print_list_dbg("array_init_list <= { array_init_list }", $$);
} | array_init_list ',' expr {
    Node* delim = node_new_clone_str(",");

    $$ = concat(3, $1, delim, $3);

    node_print_list_dbg("array_init_list <= array_init_list , expr", $$);
} | array_init_list ',' '{' array_init_list '}' {
    Node* lp = node_new_clone_str(",{");
    Node* rp = node_new_clone_str("}");

    $$ = concat(4, $1, lp, $4, rp);

    node_print_list_dbg("array_init_list <= array_init_list , { array_init_list }", $$);
}

 /* Scalar declaration */
scalar_decl: TYPE idents ';' {
    $$ = $2;
    node_print_list_dbg("scalar_decl <= TYPE idents ;", $$);
}

idents: ident {
    $$ = $1;
    node_print_list_dbg("idents <= ident", $$);
} | idents ',' ident {
    Node* delim = node_new_clone_str(",");

    $$ = concat(2, $1, $3);
    node_print_list_dbg("idents <= idents , ident", $$);
}

ident: IDENT {
    fn_locals_count += 1;
    var_table_add($1->str);

    dbg("ident <= IDENT");
    $$ = new Node("");
} | IDENT '=' expr {
    fn_locals_count += 1;
    var_table_add($1->str);

    var_table_dump();
    std::stringstream ss;
    ss << "\n# Store from top to stack bottom\n";
    ss << "lw t0, 0(sp)\n";
    ss << "sw t0, -" << (48 + 4 * var_table_get($1->str)) << "(s0)\n";

    $$ = concat(2, $3, new Node(ss.str()));
    node_print_list_dbg("ident <= IDENT = expr", $$);
}

 /* Const declaration */

const_decl: CONST TYPE const_idents ';' {
    Node* opentag = node_new_clone_str("<const_decl>const");
    Node* closetag = node_new_clone_str(";</const_decl>");

    $$ = concat(4, opentag, $2, $3, closetag);
    node_print_list_dbg("const_decl <= TYPE const_idents ;", $$);
}

const_idents: const_ident {
    node_print_list_dbg("const_idents <= const_ident", $1);
    $$ = $1;
} | const_idents ',' const_ident {
    dbg("const_idents: const_idents , const_ident");
    Node* delim = node_new_clone_str(",");

    $$ = concat(3, $1, delim, $3);
}

const_ident: IDENT {
    dbg("const_ident: IDENT");
    $$ = $1;
} | IDENT '=' expr {
    Node* op = node_new_clone_str("=");

    $$ = concat(3, $1, op, $3);
    node_print_list_dbg("const_ident <= IDENT = expr", $$);
}

 /* If statements */
if_stmt: IF '(' expr ')' comp_stmt {
    auto label = next_label_name("__IF_TAIL_");
    Node* if_head = nullptr;
    Node* if_tail = nullptr;

    {
        std::stringstream ss;
        ss << "\n# IF statement head check\n";
        ss << "lw t0, 0(sp)\n";
        ss << "addi sp, sp, 4\n";
        ss << "beqz t0, " << label << "\n";
        if_head = new Node(ss.str());
    }

    {
        std::stringstream ss;
        ss << "\n" << label << ":\n";
        if_tail = new Node(ss.str());
    }

    $$ = concat(4, $3, if_head, $5, if_tail);
    node_print_list_dbg("if <= IF ( expr ) comp_stmt", $$);
}

if_else_stmt: IF '(' expr ')' comp_stmt ELSE comp_stmt {
    auto label = next_label_name("__ELSE_TAG_");
    Node* if_head = nullptr;
    Node* else_head = nullptr;

    {
        std::stringstream ss;
        ss << "\n# IF statement head check\n";
        ss << "lw t0, 0(sp)\n";
        ss << "addi sp, sp, 4\n";
        ss << "beqz t0, " << label << "\n";
        if_head = new Node(ss.str());
    }

    {
        std::stringstream ss;
        ss << "\n" << label << ":\n";
        else_head = new Node(ss.str());
    }

    $$ = concat(5, $3, if_head, $5, else_head, $7);
    node_print_list_dbg(
        "if_else_stmt <= IF ( expr ) comp_stmt ELSE comp_stmt",
        $$
    );
}

 /* Switch statements */
swt_stmt: SWT '(' expr ')' '{' swt_cl_list '}' {
    Node* swit = node_new_clone_str("switch(");
    Node* lp = node_new_clone_str("){");
    Node* rp = node_new_clone_str("}");

    if ($6 == NULL) {
        $$ = concat(4, swit, $3, lp, rp);
    } else {
        $$ = concat(5, swit, $3, lp, $6, rp);
    }
    node_print_list_dbg("swt_stmt <= SWT ( expr ) { swt_cl_list }", $$);
}

swt_cl_list: /* empty */ {
    $$ = NULL;
} | swt_cl_list swt_cl {
    if ($1 == NULL) {
        $$ = $2;
    } else {
        $$ = concat(2, $1, $2);
    }
    node_print_list_dbg("swt_cl_list <= swt_cl_list swt_cl", $$);
}

swt_cl: CASE signed_intlit ':' swt_line_list {
    Node* cas = node_new_clone_str("case");
    Node* colon = node_new_clone_str(":");

    if ($4 == NULL) {
        $$ = concat(3, cas, $2, colon);
    } else {
        $$ = concat(4, cas, $2, colon, $4);
    }
    node_print_list_dbg("swt_cl <= CASE INTLIT : swt_line_list", $$);
} | DFT ':' swt_line_list {
    Node* dft = node_new_clone_str("default:");

    if ($3 == NULL) {
        $$ = dft;
    } else {
        $$ = concat(2, dft, $3);
    }
    node_print_list_dbg("swt_cl <= DFT : swt_line_list", $$);
}

swt_line_list: /* empty */ {
    $$ = NULL;
} | swt_line_list stmt {
    if ($1 == NULL) {
        $$ = $2;
    } else {
        $$ = concat(2, $1, $2);
    }
    node_print_list_dbg("swt_line_list <= swt_line_list stmt", $$);
}

/* While statements */
while_stmt: WHL '(' expr ')' stmt {
    auto head_label = next_label_name("__WHILE_HEAD_");
    auto tail_label = next_label_name("__WHILE_TAIL_");
    Node* while_head = nullptr;
    Node* while_mid  = nullptr;
    Node* while_tail = nullptr;

    {
        std::stringstream ss;
        ss << head_label << ":\n# while predicate expr";
        while_head = new Node(ss.str());
    }
    
    // expr is put on top of stack

    {
        std::stringstream ss;
        ss << "\n# while statement head check\n";
        ss << "lw t0, 0(sp)\n";
        ss << "addi sp, sp, 4\n";
        ss << "beqz t0, " << tail_label << "\n";
        while_mid = new Node(ss.str());
    }

    {
        std::stringstream ss;
        ss << tail_label << ":\n";
        ss << "j " << head_label << "\n";
        while_tail = new Node(ss.str());
    }

    $$ = concat(5, while_head, $3, while_mid, $5, while_tail);
    node_print_list_dbg("while_stmt <= WHL ( expr ) stmt", $$);
}

dowhile_stmt: DO stmt WHL '(' expr ')' ';' {
    auto head_label = next_label_name("__DOWHILE_HEAD_");
    Node* dowhile_head = nullptr;
    Node* dowhile_tail = nullptr;

    {
        std::stringstream ss;
        ss << head_label << ":\n";
        dowhile_head = new Node(ss.str());
    }
    
    // stmt is run
    // expr is put on top of stack

    {
        std::stringstream ss;
        ss << "\n# dowhile condition check\n";
        ss << "lw t0, 0(sp)\n";
        ss << "addi sp, sp, 4\n";
        ss << "bnez t0, " << head_label << "\n";
        dowhile_tail = new Node(ss.str());
    }

    $$ = concat(4, dowhile_head, $2, $5, dowhile_tail);
    node_print_list_dbg("dowhile_stmt <= DO stmt WHL ( expr );", $$);
}

 /* For statements */
for_stmt: FOR '(' opt_expr ';' opt_expr ';' opt_expr ')' stmt {
    Node* lp = node_new_clone_str("for(");
    Node* delim_a = node_new_clone_str(";");
    Node* delim_b = node_new_clone_str(";");
    Node* rp = node_new_clone_str(")");

    $$ = concat(8, lp, $3, delim_a, $5, delim_b, $7, rp, $9);
    node_print_list_dbg("for_stmt <= FOR ( opt_expr *3 ) stmt", $$);
}

opt_expr: /* empty */ {
    $$ = NULL;
} | expr {
    $$ = $1;
}

 /* Other statements */
ret_stmt: RET expr ';' {
    Node* ret = node_new_clone_str("return");
    Node* colon = node_new_clone_str(";");

    $$ = concat(3, ret, $2, colon);
} | RET ';' {
    $$ = node_new_clone_str("return;");
}

brk_stmt: BRK ';' {
    $$ = node_new_clone_str("break;");
}

cont_stmt: CONT ';' {
    $$ = node_new_clone_str("continue;");
}

%%

int main() {
    yyparse();
    return 0;
}

void output_list(Node* head) {
    node_print_list(head);
}

int yyerror(char* err) {
    fprintf(stderr, "%s\n", err);
    return 0;
}
