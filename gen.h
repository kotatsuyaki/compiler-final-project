#ifndef GEN_H
#define GEN_H

#include "./support.h"

#include <cstddef>
#include <vector>
#include <sstream>
#include <unordered_map>
#include <vector>
#include <string>

// mode 1: spike emulator
// mode 0: riscv arduino
#define GENMODE 0

#if GENMODE == 0

static const char* global_head_str = R"(
.global codegen
codegen:
)";

static const char* callee_entry_str = R"(
# Function prolog
sw s0, -4(sp)
addi sp, sp, -4
addi s0, sp, 0

sw sp, -4(s0)
sw s1, -8(s0)
sw s2, -12(s0)
sw s3, -16(s0)
sw s4, -20(s0)
sw s5, -24(s0)
sw s6, -28(s0)
sw s7, -32(s0)
sw s8, -36(s0)
sw s9, -40(s0)
sw s10, -44(s0)
sw s11, -48(s0)

addi sp, s0, -48

)";

static const char* callee_leave_str = R"(
# Function epilog
lw s11, -48(s0)
lw s10, -44(s0)
lw s9, -40(s0)
lw s8, -36(s0)
lw s7, -32(s0)
lw s6, -28(s0)
lw s5, -24(s0)
lw s4, -20(s0)
lw s3, -16(s0)
lw s2, -12(s0)
lw s1, -8(s0)
lw sp, -4(s0)

addi sp, sp, 4
lw s0, -4(sp)

jalr x0, 0(x1)

)";

static const char* additional_fn = R"(
# No additional section
)";

#endif

#if GENMODE == 1

static const char* global_head_str = R"(
.text
.global codegen
.global _start
codegen:
)";

static const char* callee_entry_str = R"(
# Function prolog
sw s0, -4(sp)
addi sp, sp, -4
addi s0, sp, 0

sw sp, -4(s0)
sw s1, -8(s0)
sw s2, -12(s0)
sw s3, -16(s0)
sw s4, -20(s0)
sw s5, -24(s0)
sw s6, -28(s0)
sw s7, -32(s0)
sw s8, -36(s0)
sw s9, -40(s0)
sw s10, -44(s0)
sw s11, -48(s0)

addi sp, s0, -48

)";

static const char* callee_leave_str = R"(
# Function epilog
lw s11, -48(s0)
lw s10, -44(s0)
lw s9, -40(s0)
lw s8, -36(s0)
lw s7, -32(s0)
lw s6, -28(s0)
lw s5, -24(s0)
lw s4, -20(s0)
lw s3, -16(s0)
lw s2, -12(s0)
lw s1, -8(s0)
lw sp, -4(s0)

addi sp, sp, 4
lw s0, -4(sp)

jalr x0, 0(x1)

)";

static const char* additional_fn = R"(
# Additional
delay:
jalr x0, 0(x1)
digitalWrite:
jalr x0, 0(x1)

_start:
jal ra, codegen
infloop:
nop
jal ra, infloop
)";

#endif

struct SymtblEntry {
    std::string name;
    size_t scope;
    size_t pos;
    SymtblEntry(std::string name, size_t scope, size_t pos);
};

extern size_t cur_scope;
extern size_t next_local_pos;

extern std::vector<SymtblEntry> var_table;

extern size_t fn_decl_args_count;
extern size_t fn_locals_count;

extern std::unordered_map<std::string, size_t> fn_table;

void var_table_add(std::string name);
size_t var_table_get(std::string name);
void var_table_dump();

std::string next_label_name(std::string prefix);

Node* get_loadvar_node(std::string name);

void output_to_file(Node* head);

#endif /* ifndef GEN_H */
