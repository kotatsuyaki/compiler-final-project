#include "./gen.h"
#include "./support.h"

#include <fstream>
#include <limits>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

namespace {
std::vector<std::unordered_map<std::string, SymtblEntry>> symtbl;
} // namespace

size_t fn_locals_count = 0;
size_t fn_args_count = 0;
size_t fn_decl_args_count = 0;

size_t cur_scope = 0;

std::vector<SymtblEntry> var_table{};

SymtblEntry::SymtblEntry(std::string name, size_t scope, size_t pos)
    : name(name), scope(scope), pos(pos) {}

std::unordered_map<std::string, size_t> fn_table{
    {"delay", 1},
    {"digitalWrite", 2},
};

void var_table_add(std::string name) {
    size_t pos = 0;
    if (var_table.empty()) {
        pos = 1;
    } else {
        pos = var_table.back().pos + 1;
    }
    var_table.emplace_back(name, cur_scope, pos);
}

size_t var_table_get(std::string name) {
    for (auto it = var_table.rbegin(); it != var_table.rend(); it++) {
        if (it->name == name) {
            return it->pos;
        }
    }
    return std::numeric_limits<size_t>::max();
}

void var_table_dump() {
    for (auto&& el : var_table) {
        std::cerr << el.name << ": " << el.scope << ", pos " << el.pos << "\n";
    }
}

std::string next_label_name(std::string prefix) {
    static size_t counter = 0;
    std::stringstream ss;
    ss << prefix << counter++;
    return ss.str();
}

Node* get_loadvar_node(std::string name) {
    auto local_pos = var_table_get(name);

    std::stringstream ss;
    ss << "# Load variable " << name << "\n";
    ss << "lw t0, -" << (local_pos * 4 + 48) << "(s0)\n";
    ss << "sw t0, -4(sp)\n";
    ss << "addi sp, sp, -4\n";

    return new Node(ss.str());
}

void output_to_file(Node* head) {
    std::cerr << "Dump to file...\n";

    std::fstream codegen_file("./codegen.S", std::fstream::out);
    while (head) {
        codegen_file << head->str;
        head = head->next;
    }
    codegen_file.flush();
    codegen_file.close();
}
